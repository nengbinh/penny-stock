// install and require module
const express = require("express");
const path = require('path');
    // routes setup
    var indexRouter = require('./routes/index');
//
var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);


// server runs
app.listen(8009, function() {
    console.log("listening on port 8009");
});

