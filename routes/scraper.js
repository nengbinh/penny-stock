var request = require('request');

function getPage(url) {
    return new Promise(function(resolve, reject){
        request({
            url:    url,
            method: 'GET',
            headers: {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.'}
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                try{
                    resolve(body);
                } catch(e) {
                    reject(e);
                }
            }
        });
    });
}

module.exports = getPage;
