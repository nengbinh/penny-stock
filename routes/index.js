const express = require('express');
const router = express.Router();
const cheerio = require('cheerio');
const async = require('async');
var request = require('request');
var temp = {};

// request index page
router.get('/', function(req,res){
    res.sendFile('index.html', {root: './public/'})
});

// start to scrape data
router.get('/scrape', function(req,res){
    const s = require('./scraper');

    async.waterfall([
        function (done) {
            process_part_1(s, done);
        },
        function (p1_ticker_list, done) {
            process_part_2(s, p1_ticker_list, done);
        },
        function (p1_ticker_list, done) {
            process_part_3(s, p1_ticker_list, done);
        }
    ], function(err, result){
        console.log('process_done');
        res.json(temp);
    })


});

// Load Page - Get Ticker List
function process_part_1(s, done) {
     s('https://finviz.com/').then(function(val){
        $ = cheerio.load(val);
        // get table list
        const table = $('#homepage table tr').eq(5).find('tr td[width="50%"]');
        const t1 = table.first();
        const t2 = table.last();
        // get all the Ticker names
        var p1_ticker_list = [...map_ticker_name(t1), ...map_ticker_name(t2)];
        console.log('part_1_done');
        // p1_ticker_list = ['KRTX'];
        return done(null, [...new Set(p1_ticker_list)]);
    });
}

// Get Ticker's Detail information - https://finviz.com/
function process_part_2(s, p1_ticker_list, done) {
    var date = getDate();
    const p1_ticker_list_length = p1_ticker_list.length;
    var c = 0;
    //
    p1_ticker_list.forEach(function(val, index, elem) {
        var ticker = val;
        request({
            url:    'https://finviz.com/quote.ashx?t=' + val,
            method: 'GET',
            headers: {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.'}
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                $ = cheerio.load(body);
                temp[val] = {}; // init a new obj

                // push it into obj
                var titles = $('td[class="snapshot-td2-cp"]');
                var text = $('td[class="snapshot-td2"] b');
                text.each(function(index, el){
                    //
                    var a = titles[index]['children'][0]['data'];
                    //
                    temp[val][a] = $(this).text();
                });

                // get ticker,index, date, Selector
                temp[val]['Ticker'] = val;
                temp[val]['Index'] = $('span[class="body-table"]').text();
                temp[val]['Date'] = date;
                temp[val]['Selector'] = $('td[class="fullview-links"]').eq(1).text();

                c++;
                console.log('part2 - ', c, p1_ticker_list_length, val);
                // check if done
                if( c === p1_ticker_list_length){
                    console.log('part_2_done');
                    return done(null, p1_ticker_list);
                }
                // -----------------------------------
            }
        });
    });
}

// Get Ticker's Detail information - https://www.barchart.com
function process_part_3(s, p1_ticker_list, done) {
    const p1_ticker_list_length = p1_ticker_list.length;
    var c = 0;
    //
    p1_ticker_list.forEach(function(val, index, elem) {
        var ticker = val;
        request({
            url:    'https://money.cnn.com/quote/quote.html?symb='+ val,
            method: 'GET',
            headers: {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.'}
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                $ = cheerio.load(body);

                var d = $('td[class="wsod_quoteDataPoint"]');
                temp[val]['Previous Close'] = d.eq(0).text();
                temp[val]['Today Open'] = d.eq(1).text();
                temp[val]['Day Range'] = d.eq(2).text();
                c++;
                console.log('part3 - ', c, p1_ticker_list_length, val);

                // check if done
                if( c === p1_ticker_list_length){
                    console.log('part_3_done');
                    return done(null);
                }
                // -----------------------------------
            }
        });
    });
}


// get all the Ticker names
function map_ticker_name(elem) {
    var list = [];
    elem.find('tr').each(function(index, el){
        var ticker = $(this).children().first().text();
        if (ticker !== 'Ticker') {
            list.push(ticker);
        }
    });
    return list;
}

function getDate() {
    // current timestamp in milliseconds
    let ts = Date.now();
    //
    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    // prints date & time in YYYY-MM-DD format
    return month + "-" + date + "-" + year;
}


module.exports = router;
