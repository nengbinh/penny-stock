const titles = [
    'Ticker', 'Index', 'Date', 'Selector',
    'Previous Close', 'Today Open', 'Day Range',
    'Market Cap', 'Shs Float', 'Shs Outstand', 'Short Float',
    'Volume', 'Perf Week', 'Price', 'Change', 'Earnings'
];

function display_title(data, table) {
    var titles = $("<tr></tr>");
    $(data).each(function(index, ele){
        titles.append($('<th>'+ele+'</th>'));
    });
    $(table).append(titles);
    return table;
}

function display_data(data, t, table){
    Object.keys(data).forEach( function(val, index) {
        // val = key
        var detail = $("<tr></tr>");
        //
        $(t).each(function(index, ele){
            detail.append($("<td>"+data[val][ele]+"</td>"))
        });
        $(table).append(detail);
    });
    return table;
}

function display_process(data) {
    // create element
    var table = $("<table rules='rows'></table>")
            // titles element
            table = display_title(titles, table);
            // data element
            table = display_data(data, titles, table);
    //
    clearInterval(clock);
    time_setter('( Scrape success! )', 'green');
    $('body').append(table);
}

var clock;
var time;

function display_time() {
    time = 60;
    time_setter('', 'green');
    clock = setInterval(time_processor, 1000);
}

function time_processor() {
    if(time <= 0 ){
        clearInterval(clock);
        time_setter('( Scraping data failed... )', 'red');
        return false;
    }
    time_setter('( Scraping data ... please wait ' + time + 's )');
    time--;
}

function time_setter(text, color) {
    $('#time').text(text);
    if (color) {
        $('#time').css('color', color);
    }
}

function autocompleted() {
    display_time();
    $.ajax({
        type: "GET",
        url: "/scrape",
        success: function(data){
            display_process(data);
        }
    });
}
